<?php
/**
 * Created by PhpStorm.
 * User: wojciechbaszkiewicz
 * Date: 16.02.2016
 * Time: 18:46
 */


define('FILE_NBP_MAP', 'http://www.nbp.pl/kursy/xml/dir.txt');
define('FILE_NBP_PATH', 'http://www.nbp.pl/kursy/xml/');

class nbp
{
    private $type = 'a';
    private $aXMLFiles = array();
    private $rates = array();
    private $numbers = array();

    public function __construct()
    {
        $fileTxt = fopen(FILE_NBP_MAP, "r");

        while (!feof($fileTxt)) {
            $line = fgets($fileTxt);
            if ($line[0] === $this->type) {
                $this->aXMLFiles[] = substr($line, 0, strlen($line) - 2);
            }
            if ($line === false) {
                throw new Exception("File read error");
            }
        }
    }

    public function getRates()
    {
        foreach ($this->aXMLFiles as $key => $fileName) {
            $fileTxt = file_get_contents(FILE_NBP_PATH . $fileName . '.xml');
            $elXML = new SimpleXMLElement($fileTxt);
            $this->rates[$key] = array(
                'numer_tabeli' => (string)$elXML->numer_tabeli,
                'data_publikacji' => (string)$elXML->data_publikacji
            );

            foreach ($elXML->pozycja as $val) {
                $code = (string)$val->kod_waluty;
                if ($code === 'USD') {
                    $this->rates[$key][$code] = array(
                        'nazwa' => (string)$val->nazwa_waluty,
                        'ilosc' => (string)$val->przelicznik,
                        'kurs_sredni' => (string)$val->kurs_sredni
                    );
                    $this->numbers[] = str_replace(",", ".", $val->kurs_sredni);
                }
            }
        }

        echo 'Minimalny kurs USD: ' . min($this->numbers) . '<br/>';
        echo 'Maksymalny kurs USD: ' . max($this->numbers);
    }
}
?>