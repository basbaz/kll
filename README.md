README

Zadanie 1

Z kursów walut dostępnych na stronie NBP: http://www.nbp.pl/home.aspx?f=/kursy/instrukcja_pobierania_kursow_walut.html Wyszukać najwyższy i najniższy kurs USD w bieżącym roku. Wynik wypisać na ekranie.

Zadanie 2

Stworzyć klasę Dokument, która ma atrybuty numer i data i metodę drukuj(). Drukuj zwraca string z informacjami o dokumencie "Numer z dnia data", gdzie za Numer i data należy podstawić atrybuty dokumentu.
Stworzyć dwa obiekty klasy dokument: - numer: 1/2016 data: 2016-01-01 - numer: 2/2016 data: 2016-01-02
Zapisać obiekty do bazy danych SQL.
Wyciągnąć wszystkie dokumenty z bazy danych i wypisać na ekranie. Oprócz kodu php dołączyć skrypt SQL tworzący strukturę bazy danych.