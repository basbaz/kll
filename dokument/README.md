Zadanie 2
---------
1. Stworzyć klasę Dokument, która ma atrybuty numer i data i metodę drukuj().
   Drukuj zwraca string z informacjami o dokumencie "Numer z dnia data",
   gdzie za Numer i data należy podstawić atrybuty dokumentu.
2. Stworzyć dwa obiekty klasy dokument:
        - numer: 1/2016 data: 2016-01-01
        - numer: 2/2016 data: 2016-01-02
3. Zapisać obiekty do bazy danych SQL.
4. Wyciągnąć wszystkie dokumenty z bazy danych i wypisać na ekranie.
Oprócz kodu php dołączyć skrypt SQL tworzący strukturę bazy danych.