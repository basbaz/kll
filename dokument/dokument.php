<?php

/**
 * Created by PhpStorm.
 * User: wojciechbaszkiewicz
 * Date: 18.02.2016
 * Time: 20:25
 */
class Dokument
{
    private $numer = null;
    private $data = null;

    public function __construct($numer, $data)
    {
        $this->numer = $numer;
        $this->data = $data;
    }

    public function save()
    {
        $values = "'" . $this->numer . "', '" . $this->data . "'";

        try {
            $conn = new PDO('mysql:host=127.0.0.1;dbname=kll', 'user', 'password');
            $sql = "INSERT INTO dokument (numer, data) VALUES ( $values )";
            $conn->query($sql);
            $conn = null;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public static function drukuj()
    {
        try {
            $conn = new PDO('mysql:host=127.0.0.1;dbname=kll', 'user', 'password');
            echo '<table><tr><th>Numer</th><th>Data</th></tr>';
            foreach ($conn->query('SELECT * from dokument') as $row) {
                echo '<tr><td>' . $row['numer'] . '</td><td> ' . date('Y-m-d', strtotime($row['data'])) . '</td></tr>';
            }
            echo '</table>';
            $conn = null;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}
